package example

import org.scalatest._
import scala.collection.MapView

class TextSampleSpec extends FlatSpec with Matchers {
  val parsing = """[ ,.]"""
  val words = Data.text.split(parsing)
  
  def wordFrequency: MapView[String, Int] = {
    words.groupBy(identity).view.mapValues(_.length)
  }
  
  def sortedWordFrequency(ascending: Boolean = false): Seq[(String, Int)] = {
    if(ascending) wordFrequency.toSeq.sortWith(_._2 < _._2)
    else wordFrequency.toSeq.sortWith(_._2 > _._2)
  }
  
  "The imported text" should "have have a count of 260 words" in {
    val count = Data.text.split(parsing).size
    count shouldBe 260
  }

  "The imported text" should "contain a count of 22 sentences" in {
    val sentences = Data.text.split('.').size
    sentences shouldBe 22
  }

  "The longest word in the text" should "have a count of 15 characters"  in {
    val largest = words.map(_.length).max
    largest shouldBe 15 
  }

  "Counting word frequency" should "taking a list of the 6 most frequent occuring words" in {
    val mostOccurances = sortedWordFrequency().take(6)
    mostOccurances(0)._2 shouldBe 7
    mostOccurances(0)._1 should equal("non")
    mostOccurances.foreach(println) // FEEDBACK: display the 6 most frequent words
  }

  "The number of unique words in the text" should "have a count of 192" in {
    val distinctWords: Int = wordFrequency.size
    distinctWords shouldBe 192
  }
}
